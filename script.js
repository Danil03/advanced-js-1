class Employee {
    constructor(name, age, salary) {
        this.employeeName = name;
        this.employeeAge = age; 
        this.employeeSalary = salary; 
    }
    get employeeName() {
        return this.name;
    }

    set employeeName(newName) {
        newName = newName.trim();
        if (newName === '') {
            throw new Error('Ім\'я не може бути порожнім');
        }
        this.name = newName;
    }

    get employeeAge() {
        return this.age;
    }

    set employeeAge(newAge) {
        if (typeof newAge !== 'number' || newAge <= 0) {
            throw new Error('Неправильне значення віку');
        }
        this.age = newAge;
    }
    get employeeSalary() {
        return this.salary;
    }

    set employeeSalary(newSalary) {
        if (typeof newSalary !== 'number' || newSalary < 0) {
            throw new Error('Неправильне значення зарплати');
        }
        this.salary = newSalary;
    }
}  


class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get progSal() {
        return this.salary * 3;
    }
}


let prog = new Programmer("Даня", 19, 25000, "JavaScript");

let firstEmp = new Employee("Даня", 19, 25000);
console.log(firstEmp,'Salary x3:', prog.progSal);

let andr = new Employee ('Андрій', 28, 50000)
console.log(andr, 'Salary x3:', andr.progSal);

let vasil = new Programmer("Василь", 19, 30000, ["JavaScript", "Python", "Java"]);
console.log(vasil, 'Salary x3:', vasil.progSal);

let programmers = [
    new Programmer("Даня", 19, 25000, ["JavaScript", "Python"]),
    new Programmer("Андрій", 25, 30000, ["Java", "C++"]),
    new Programmer("Сергій", 30, 35000, ["Ruby", "PHP"])
];

console.log( programmers, programmers.progSal);

// 1.Прототипне наслідування в JavaScript дозволяє "копіювати" властивості та методи одного об'єкта для використання в іншому об'єкті.
// 2.Конструктор super() використовується для виклику конструктора батьківського класу у підкласі. Коли ми викликаємо super() у конструкторі 
// підкласу, ми насправді викликаємо конструктор батьківського класу з тими самими аргументами, які були передані в конструктор підкласу.

